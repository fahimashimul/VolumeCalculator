<?php
include_once "vendor/autoload.php";
use Pondit\Calculator\VolumeCalculator\Volume;
use Pondit\Calculator\VolumeCalculator\Cube;
use Pondit\Calculator\VolumeCalculator\Cylinder;
use Pondit\Calculator\VolumeCalculator\Cone;

$volume=new Volume();
var_dump($volume);

$cube=new Cube();
var_dump($cube);

$cylinder=new Cylinder();
var_dump($cylinder);

$cone=new Cone();
var_dump($cone);